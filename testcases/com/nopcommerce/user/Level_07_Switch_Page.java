package com.nopcommerce.user;

import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.BaseTest;
import commons.PageGeneratorManager;
import pageObjects.users.UserAddressPageObject;
import pageObjects.users.UserCustomerInfoPageObject;
import pageObjects.users.UserHomePageObject;
import pageObjects.users.UserLoginPageObject;
import pageObjects.users.UserOrderPageObject;
import pageObjects.users.UserRegisterPageObject;
import pageObjects.users.UserRewardPointPageObject;

public class Level_07_Switch_Page extends BaseTest {
	private WebDriver driver;
	private UserHomePageObject homePage;
	private UserRegisterPageObject registerPage;
	private UserLoginPageObject loginPage;
	private UserCustomerInfoPageObject customerInfoPage;
	private UserOrderPageObject orderPage;
	private UserAddressPageObject addressPage;
	private UserRewardPointPageObject rewardPoitPage;
	private String emailAddress, lastName, firstName, password;


	@Parameters({"browser", "url"})
	@BeforeClass
	public void beforeClass(String browserName, String url) {
		// Open URL -> open HomePage
		driver = getBrowserDriver(browserName, url);
		homePage = PageGeneratorManager.getUserHomePage(driver);
		
		firstName = "Lionel";
		lastName = "Messi";
		emailAddress = "LionelMessi" + getRandomNumber() + "@gmail.com";
		password = "123456";
	}

	@Test
	public void User_01_Register_To_System() {
		registerPage = homePage.clickToRegisterLink();
		
		registerPage.inputToFirsNameTextbox(firstName);
		registerPage.inputToLastNameTextbox(lastName);
		registerPage.inputToEmailTextbox(emailAddress);
		registerPage.inputToPasswordTextbox(password);
		registerPage.inputToConfirmPasswordTextbox(password);
		registerPage.clickToRegisterButton();
		
		Assert.assertEquals(registerPage.getRegisterSuccessMessage(), "Your registration completed");
		
		// Từ trang Register chuyển qua Home page
		homePage = registerPage.clickContinueBtn();
	}

	@Test
	public void User_02_Login_To_System() {
		// Từ trang Home chuyển qua Login page
		loginPage = homePage.clickToLoginLink();
		
		loginPage.inputToEmailTextbox(emailAddress);
		loginPage.inputToPasswordTextbox(password);
		
		// Từ trang Login page chuyển qua Home page
		homePage = loginPage.clickToLoginBtn();
	}

	@Test
	public void User_03_My_Account_Infomation() {
		// Từ trang Home chuyển qua Custome Info page
		customerInfoPage = homePage.clickToMyAccountLink();
		
		Assert.assertEquals(customerInfoPage.getFirstNameTextboxValue(), firstName);
		Assert.assertEquals(customerInfoPage.getLastNameTextboxValue(), lastName);
		Assert.assertEquals(customerInfoPage.getEmailTextboxValue(), emailAddress);
	}

	@Test
	public void User_04_Navigate_Page() {
		// Customer info Page -> Order page
		orderPage = customerInfoPage.openOrderPage(driver);
		
		// Order Page -> Reward point
		rewardPoitPage = orderPage.openRewardPointPage(driver);
		
		// Reward point -> Order page
		orderPage = rewardPoitPage.openOrderPage(driver);
		
		// Order page -> Customer info page
		customerInfoPage = orderPage.openCustomerInfoPage(driver);
		
		// Customer info page -> Address page
		addressPage = customerInfoPage.openAddressPage(driver);
		
		// Address page -> Reward point
		rewardPoitPage = addressPage.openRewardPointPage(driver);
		
	}

	public int getRandomNumber() {
		Random rand = new Random();
		return rand.nextInt(999);
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}