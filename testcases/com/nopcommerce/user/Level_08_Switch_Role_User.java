package com.nopcommerce.user;

import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.BaseTest;
import commons.GlobalConstants;
import commons.PageGeneratorManager;
import pageObjects.admin.AdminLoginPageObject;
import pageObjects.users.UserAddressPageObject;
import pageObjects.users.UserCustomerInfoPageObject;
import pageObjects.users.UserHomePageObject;
import pageObjects.users.UserLoginPageObject;
import pageObjects.users.UserOrderPageObject;
import pageObjects.users.UserRegisterPageObject;
import pageObjects.users.UserRewardPointPageObject;

public class Level_08_Switch_Role_User extends BaseTest {
	WebDriver driver;
	UserLoginPageObject userLoginPage;
	UserHomePageObject userHomePage;
	AdminLoginPageObject adminLoginPage;

	String userEmailAddress, userPassword;

	@Parameters({"browser"})
	@BeforeClass																																																										
	public void beforeClass(String browserName) {

		// 2- Quyền User mở url lên -> qua trang login
		driver = getBrowserDriver(browserName, GlobalConstants.USER_URL);
		userHomePage = PageGeneratorManager.getUserHomePage(driver);
		
		userEmailAddress = "dannt@gmail.com";
		userPassword = "123456";
		
	}

	@Test
	public void Role_01_Switch_User_To_Admin() {
		// Home -> Login
		userLoginPage = userHomePage.clickToLoginLink();
		
		// Login vào -> 

		userHomePage = userLoginPage.loginAsUser(userEmailAddress, userPassword);
		
		// Home (User) -> Mở ra Admin url -> Login Admim
		userHomePage.openPageUrl(driver, GlobalConstants.ADMIN_URL);
		adminLoginPage = PageGeneratorManager.getAdminLoginPage(driver);
		
		// Login -> Dashboard
		
		//Dashboard -> Any Page
		}

	@Test
	public void User_02_Switch_Admin_To_User() {

	}

	
	public int getRandomNumber() {
		Random rand = new Random();
		return rand.nextInt(999);
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}