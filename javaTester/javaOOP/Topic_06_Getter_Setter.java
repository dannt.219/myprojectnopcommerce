package javaOOP;

public class Topic_06_Getter_Setter {



    public String personName;
    public int personAge;
    public long personPhone;
    public float personAmountBank;

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        if (personName == null || personName.isBlank() || personName.isEmpty()) {
            throw  new IllegalArgumentException("Nhập đúng tên đi bạn ơi!");
        } else {
            this.personName = personName;
        }

    }

    public int getPersonAge() {
        return personAge;
    }

    public void setPersonAge(int personAge) {
        if (personAge > 18 && personAge < 50) {
            throw new IllegalArgumentException("Tuổi chưa đủ hoặc thừa con mẹ nó rồi!");
        } else {
            this.personAge = personAge;
        }
    }

    public long getPersonPhone() {
        return personPhone;
    }

    public void setPersonPhone(long personPhone) {
        if (!String.valueOf(personPhone).startsWith("0")) {
            throw new IllegalArgumentException("Số điện thoại phải bắt đầu bằng số 0");
        } else if (personPhone < 10 || personPhone > 12){
            throw new IllegalArgumentException("Số điện thoại phải từ 10-11 số nha cmm");
        } else {
            this.personPhone = personPhone;
        }
    }

    public float getPersonAmountBank() {
        return personAmountBank;
    }

    public void setPersonAmountBank(float personAmountBank) {
        this.personAmountBank = personAmountBank;
    }
    //Setter
//    public void setPersonName(String personName) {
//        if (personName == null || personName.isEmpty() || personName.isBlank()) {
//            throw new IllegalArgumentException("Nhap ten khong hop le!");
//        } else {
//            this.personName =personName;
//        }
//    }
//
//    //Getter
//    public String getPersonName() {
//        return  this.personName;
//    }
}
