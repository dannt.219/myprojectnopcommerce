package javaOOP;

public class Topic_01_Overiding {
    private  int personAge;
    private  int personCup;

    public int getPersonAge() {
        return personAge;
    }

    public void setPersonAge(int personAge) {
        this.personAge = personAge;
    }

    public int getPersonCup() {
        return personCup;
    }

    public void setPersonCup(int personCup) {
        this.personCup = personCup;
    }
    public static void main(String[] args) {
        System.out.println("");
    }
}
