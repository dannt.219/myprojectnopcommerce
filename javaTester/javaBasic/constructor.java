package javaBasic;

public class constructor {
	private String myField;
	private String myName;
	
	// Constructor
	public constructor(String myFields, String myNames) {
		myField = myFields;
		myName = myNames;
	}
	
	public void showInfo() {
		System.out.println("Field is: " + myField);
		System.out.println("My Name is: " + myName);
	}
	
	public static void main(String[] args) {
		constructor consinfo = new constructor("tres", "Dan Tien");
		
		consinfo.showInfo();
	}
	
	
}
