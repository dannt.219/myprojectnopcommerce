package javaBasic;

import java.util.Random;

public class RandomEmailGenerator {

	private static final String CHARACTERS = "tester12345";
	private static final String DOMAIN = "gmail.com";
	
	
	public static String generateRandomEmail() {
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 10; i++) {
			int index = random.nextInt(CHARACTERS.length());
			char randomChar = CHARACTERS.charAt(index);
			sb.append(randomChar);
		}
		sb.append("@").append(DOMAIN);
		return sb.toString();
	}
	
    public static void main(String[] args) {
        String randomEmail = generateRandomEmail();
        System.out.println("Random Email: " + randomEmail);
    }
}
