package pageObjects.users;


import org.openqa.selenium.WebDriver;

import commons.BasePage;
import commons.PageGeneratorManager;
import pageUIs.UserHomePageUI;

public class UserHomePageObject extends BasePage {
	private WebDriver driver;
	
	// Hàm khởi tạo:
	// 1. Trùng tên với tên class
	// 2. Không có kiểu trả về
	// 3. Có tham số hoặc không
	// 4. 1 hàm có thể có 1 hoặc nhiều hàm khởi tạo
	// 5. không define 1 hamgf khởi tạo thì trình biên dịch sẽ tự tạo ra 1 hàm khởi tạo mặc định không có tham số
	// 6. Luôn luôn được gọi đầu tiên khi object được new lên 
	
	public UserHomePageObject(WebDriver driver) {
		this.driver = driver;
	}
	public UserRegisterPageObject clickToRegisterLink() {
		// Cach 2 dung ke thua
		waitForElementClickable(driver, UserHomePageUI.REGISTER_LINK);
		clickToElement(driver, UserHomePageUI.REGISTER_LINK);
		return PageGeneratorManager.getUserRegisterPage(driver);
	}
	public UserLoginPageObject clickToLoginLink() {
		
		waitForElementClickable(driver, UserHomePageUI.LOGIN_LINK);
		clickToElement(driver, UserHomePageUI.LOGIN_LINK);
		return PageGeneratorManager.getUserLoginPage(driver);
		
	}
	public UserCustomerInfoPageObject clickToMyAccountLink() {
		waitForElementClickable(driver, UserHomePageUI.MY_ACCOUNT_LINK);
		clickToElement(driver, UserHomePageUI.MY_ACCOUNT_LINK);
		return PageGeneratorManager.getUserCustomerInfoPage(driver);
	}
}
