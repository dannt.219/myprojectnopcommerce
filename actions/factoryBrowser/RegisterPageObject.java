package factoryBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class RegisterPageObject {
 private WebDriver driver;
 
 
 public RegisterPageObject(WebDriver driver) {
	this.driver = driver;
	PageFactory.initElements(driver, this);
 }
 
}
