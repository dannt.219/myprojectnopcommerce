package pageUIs;

public class BasePageUI {
	public static final String REWARD_POINT_PAGE_LINK = "//div[contains(@class,'account-navigation')]//a[text()='Reward points']";
	public static final String ORDER_PAGE_LINK = "//div[contains(@class,'account-navigation')]//a[text()='Orders']";
	public static final String ADDRESS_PAGE_LINK = "//div[contains(@class,'account-navigation')]//a[text()='Addresses']";
	public static final String CUSTOMER_INFO_PAGE_LINK = "//div[contains(@class,'account-navigation')]//a[text()='Customer info']";
}
